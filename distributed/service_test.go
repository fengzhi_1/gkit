package distributed

import (
	"context"
	"testing"

	"gitee.com/menciis/gkit/log"

	"github.com/stretchr/testify/assert"

	"gitee.com/menciis/gkit/distributed/backend/backend_redis"

	"gitee.com/menciis/gkit/distributed/broker"
	"gitee.com/menciis/gkit/distributed/controller/controller_redis"
	"gitee.com/menciis/gkit/distributed/locker/lock_ridis"
	"github.com/go-redis/redis/v8"
)

func initServer() *Server {
	opt := redis.UniversalOptions{
		Addrs: []string{"127.0.0.1:6379"},
	}
	client := redis.NewUniversalClient(&opt)
	if client == nil {
		return nil
	}
	lock := lock_ridis.NewRedisLock(client)
	bk := broker.NewBroker(broker.NewRegisteredTask(), context.Background())
	c := controller_redis.NewControllerRedis(bk, client, "test_task", "delayed")
	backend := backend_redis.NewBackendRedis(client, -1)
	return NewServer(c, backend, lock, log.NewHelper(log.With(log.DefaultLogger)), nil)
}

func TestRegisterTasks(t *testing.T) {
	t.Parallel()
	s := initServer()
	_, ok := s.GetRegisteredTask("test_task")
	assert.False(t, ok)
	err := s.RegisteredTasks(map[string]interface{}{
		"test_task": func() error { return nil },
	})
	assert.NoError(t, err)

	_, ok = s.GetRegisteredTask("test_task")
	assert.True(t, ok)
}

func TestRegisterTask(t *testing.T) {
	t.Parallel()
	s := initServer()
	_, ok := s.GetRegisteredTask("test_task")
	assert.False(t, ok)

	err := s.RegisteredTask("test_task", func() error { return nil })
	assert.NoError(t, err)

	_, ok = s.GetRegisteredTask("test_task")
	assert.True(t, ok)
}

package bind

import "gitee.com/menciis/gkit/options"

var defaultParse = []Binding{Query, FormPost, Header}

func DefaultParse(contentType string) []Binding {
	return append(defaultParse, contentTypeSelect(contentType))
}

func SetSelectorParse(bind []Binding) options.Option {
	return func(o interface{}) {
		o.(*all).selectorParse = bind
	}
}
